<?php

/**
 * @file
 * Administration pages functions.
 */

/**
 * The admin/exstore page callback form.
 */
function excart_settings_page($form, $form_state) {
  $form['excart_node_settings'] = array(
    '#type' => 'fieldset',
    '#title' => _excart_t('Node type selection'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['excart_node_settings']['excart_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => _excart_t('Select node types'),
    '#default_value' => variable_get('excart_node_types', array()),
    '#options' => node_type_get_names(),
    '#ajax' => array(
      'wrapper' => 'excart-type-wrapper',
      'callback' => 'excart_ajax_select'
    )
  );
  $form['excart_node_settings']['price'] = array(
    '#prefix' => '<div id="excart-type-wrapper">',
    '#suffix' => '</div>',
    '#type' => 'container',
  );

  if (isset($form_state['values']['excart_node_types'])) {
    $types = array_filter($form_state['values']['excart_node_types']);
  }
  else {
    $types = excart_get_product_node_types();
  }
  if ($types && ($options = _excart_get_price_fields($types))) {
    $form['excart_node_settings']['price']['excart_price_field'] = array(
      '#type' => 'select',
      '#title' => _excart_t('Select price field'),
      '#default_value' => variable_get('excart_price_field', 0),
      '#options' => $options,
      '#validated' => TRUE
    );
  }
  $form['excart_count_calc'] = array(
    '#title' => _excart_t('Select qty calculation algorithm'),
    '#type' => 'select',
    '#options' => array(
      0 => _excart_t('By position'),
      1 => _excart_t('By pcs'),
    ),
    '#default_value' => variable_get('excart_count_calc', 0),
  );
  $form['#submit'][] = 'excart_settings_page_submit';
  return system_settings_form($form);
}

function excart_ajax_select(&$form, &$form_state) {
  return $form['excart_node_settings']['price']['excart_price_field'];
}

function excart_settings_page_submit(&$form, &$form_state) {
  $types = $form_state['values']['excart_node_types'];
  $field_name = 'excart_add_to_cart';
  excart_check_add_to_cart_field();
  foreach ($types as $type => $active) {
    $instance = field_info_instance('node', $field_name, $type);
    if ($active && !$instance) {
      $instance = array(
        'field_name' => $field_name,
        'label' => 'Add to cart',
        'description' => 'Add to cart link',
        'entity_type' => 'node',
        'bundle' => $type,
      );
      // It doesn't exist. Create it.
      field_create_instance($instance);
    }
    elseif (!$active && $instance) {
      field_delete_instance($instance);
    }
  }
}

function _excart_get_price_fields($types = array()) {
  $available_fields = array();
  if (!$types) {
    $types = variable_get('excart_node_types', array());
  }
  if ($types) {
    foreach ($types as $type) {
      $fields = field_info_instances('node', $type);
      foreach ($fields as $name => $field) {
        $available_fields[$name] = $field['label'];
      }
    }
  }
  return $available_fields;
}
