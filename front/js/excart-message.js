(function ($) {
  Drupal.behaviors.ExCart = {
    attach: function (context, settings) {
      var $added = $('.finish');
      if ($added.length) {
        $added.on('click','.excart-finish-close', function(e){
          e.preventDefault();
          $added.fadeOut(500);
        });
      }
    }
  }
})(jQuery)