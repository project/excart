(function ($) {
  Drupal.behaviors.excartFormBahavior = {
    attach: function (context, settings) {
      $('.excart-add-to-cart-form').once('excart-form').each(function (i, d) {
        var $form = $(d);
        $form.on('click', '.excart-action-item', function () {
          var $item = $(this);
          var $field = $('input[name="qty"]', $form);
          var val = parseInt($field.val());
          if (!val || isNaN(val)) {
            val = 0;
          }
          switch ($item.data('action')) {
            case '+':
              val++;
              break;
            case '-':
              val--;

              break;
          }
          $field.val((val < 1) ? 1 : val);

        });
      });
    }
  };
})(jQuery);