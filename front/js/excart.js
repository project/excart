(function ($) {
  Drupal.behaviors.excartBahavior = {
    attach: function (context, settings) {
      var $cart = $('.cart-table');
      $('input.qty-field').once().on('keyup', function () {
        var $input = $(this);
        var $context = $input.parent('.qty-wrap');
        var count = parseInt($input.val());
        if (isNaN(count) || count < 0) {
          count = 1;
        }
        if ($('.excart-upd-btn', $context).length) {
          $('.excart-upd-btn', context).remove();
        }
        var $link = $('<a />').addClass('excart-upd-btn').addClass("use-ajax");
        $link.text(Drupal.t('Update'));
        var nid = $input.attr('name').split('-').pop();
        $link.attr('href', '/cart/ajax/update/' + nid + '/nojs/' + count);
        $link.appendTo($context);
        Drupal.behaviors.AJAX.attach(context, settings);
      });
    }
  };
})(jQuery);