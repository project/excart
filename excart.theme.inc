<?php

/*
 * обработка данных перед отправкой в файл темизации
 *
 */
#implements Hook_process_excart_cart()

function excart_process_excart_cart(&$vars) {
  foreach ($vars['products'] as &$product) {
    $product['options']['row_attributes'] = drupal_attributes($product['options']['row_attributes_array']);
    $product['options']['qty_attributes'] = drupal_attributes($product['options']['qty_attributes_array']);
  }
  $vars['total_formmated'] = $vars['info']['total_formatted'];
  if (!empty($vars['files']['js'])) {
    $js = $vars['files']['js'];
    if (!is_array($js)) {
      $js = array($js);
    }
    foreach ($js as $file_name) {
      drupal_add_js($file_name);
    }
  }
  if (!empty($vars['files']['css'])) {
    $css = $vars['files']['css'];
    if (!is_array($css)) {
      $css = array($css);
    }
    foreach ($css as $file_name) {
      drupal_add_css($file_name);
    }
  }
  $links = array();
  $links['return'] = array(
    'title' => _excart_t('Continue shopping'),
    'href' => '<front>',
  );
  if (module_exists('excart_order')) {
    $links['excart-order'] = array(
      'title' => _excart_t('Checkout order'),
      'href' => 'cart/checkout',
    );
  }
  if (!empty($vars['links']) && $vars['links']['#theme'] == 'links') {
    $vars['links']['#links'] += $links;
  }
  elseif (empty($vars['links'])) {
    $vars['links'] = array(
      '#theme' => 'links',
      '#links' => $links,
      '#attributes' => array(
        'id' => 'excart-links'
      )
    );
  }
}

# if you can change the price template, copy above function into your
# template.php file, change the returned data by your conditions
# and rename it THEME_NAME_excart_format_price

function theme_excart_format_price($vars) {
  $price = $vars['price'];
  $wrapper = $vars['wrapper'];
  $int_price = $price * 1;
  if (((string) $int_price) == ((string) $price )) {
    $decimals = 0;
  }
  else {
    $decimals = 2;
  }
  $price = number_format($price, $decimals, '.', ' ');
  if ($wrapper) {
    $out = array(
      '#prefix' => '<span class="price">',
      '#suffix' => '</span>'
    );
  }
  else {
    $out = array();
  }
  $out['price']['#markup'] = $price;
  return render($out);
}

function excart_process_excart_cart_block(&$vars) {
  $vars['cart_count'] = format_plural($vars['count'], '1 product, !total rub', '@count products, !total rub', array('!total' => $vars['cart']['total']));
  $vars['count_plural'] = format_plural($vars['count'], '1 product', '@count products');
  $vars['total_html'] = excart_format_price($vars['cart']['total']);
  foreach ($vars['cart']['items'] as &$item) {
    if (empty($item['price_html'])) {
      $item['price_html'] = excart_format_price($item['price']);
    }
  }
  if (!empty($vars['files']['js'])) {
    $js = $vars['files']['js'];
    if (!is_array($js)) {
      $js = array($js);
    }
    foreach ($js as $file_name) {

      drupal_add_js($file_name);
    }
  }
  if (!empty($vars['files']['css'])) {
    $css = $vars['files']['css'];
    if (!is_array($css)) {
      $css = array($css);
    }
    foreach ($css as $file_name) {
      drupal_add_css($file_name);
    }
  }
}
