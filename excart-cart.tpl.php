<div id="cart-page">
  <table class="cart-table">
    <thead>
      <tr>
        <th class="cart-title"><?php print _excart_t('Product title'); ?></th>
        <th class="cart-qty"><?php print _excart_t('Quantity'); ?></th>
        <th class="cart-price"><?php print _excart_t('Price'); ?></th>
        <th class="cart-total"><?php print _excart_t('Total'); ?></th>
        <th class="cart-action"><?php print _excart_t('Actions'); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($products as $nid => $product): ?>
        <tr<?php print $product['options']['row_attributes']; ?>>
          <td class="cart-title">
            <a href="<?php print $product['url']; ?>"><?php print $product['title']; ?></a>
          </td>
          <td class="cart-qty">
            <div class="qty-wrap">
              <?php print $product['links']['remove']; ?>
              <input name="qty-<?php print $nid; ?>" class="form-text qty-field" value="<?php print $product['qty']; ?>">
              <?php print $product['links']['add']; ?>
            </div>
          </td>
          <td class="cart-price"><?php print $product['price_html']; ?></td>
          <td class="cart-total" id="excart-total-<?php print $nid; ?>"><?php print $product['total_price']; ?></td>
          <td class="cart-actions"><?php print $product['links']['delete']; ?></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <div id="excart-total-price-wrapper">
    <div class="total-label">Итого:</div>
    <?php print render($total_formmated); ?>
  </div>
  <div class="links">
    <?php print render($links); ?>
  </div>
</div>