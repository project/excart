<?php

function excart_cart_page() {
  $cart = excart_get_cart();
  drupal_alter('excart_cart_load', $cart);

  $items = $cart['items'];
  if (empty($items)) {
    return '<div class="cart-empty">' . _excart_t('Cart is empty') . '</div>';
  }
  foreach ($items as $id => &$item) {
    $attr_plus['attributes']['class'] = array('up', 'use-ajax');
    $attr_plus['attributes']['id'] = 'excart-add-link-' . $id;

    $attr_minus['attributes']['class'] = array('down', 'use-ajax');
    $attr_minus['attributes']['id'] = 'excart-sub-link-' . $id;

    $attr_del['attributes']['class'] = array('delete', 'use-ajax');
    $attr_del['attributes']['id'] = 'excart-del-link-' . $id;

    $plus = l('+', 'cart/ajax/add/' . $id . '/nojs', $attr_plus);
    $minus = l('–', 'cart/ajax/sub/' . $id . '/nojs', $attr_minus);
    $del = l(t('Delete'), 'cart/ajax/delete/' . $id . '/nojs', $attr_del);
    $item['options'] = array(
      'row_attributes_array' => array(
        'class' => array('excart-row'),
        'id' => 'excart-row-' . $id,
      ),
      'qty_attributes_array' => array(
        'name' => 'qty_field_' . $id,
        'id' => 'qty-field-' . $id,
      ),
      'node_url' => url('node/' . $id),
    );
    $item['links'] = array(
      'add' => $plus,
      'remove' => $minus,
      'delete' => $del
    );
    $item['url'] = url('node/' . $item['nid'], array('absolute' => TRUE));
    $price = $item['price'];
    $item['price_html'] = excart_format_price($price, $id);
    $item['total_price'] = excart_format_price($price * $item['qty'], $id . "t");
    $item['node'] = node_load($item['nid']);
  }

  $count = excart_get_total_count();
  $total = excart_get_total_price();
  $total_formatted = excart_format_total($count, $total);
  $dec = is_int($total) ? 0 : 2;

  $checkout_link = l(t('Clean cart'), 'cart/clear');
  $clear_link = l(t('Clean cart'), 'cart/clear');
  $ex_path = drupal_get_path('module', 'excart') . '/front';
  $output_array = array(
    '#theme' => 'excart_cart',
    '#products' => $items,
    '#total' => $total,
    '#info' => array(
      'count' => $count,
      'total' => $total,
      'total_formatted' => $total_formatted,
    ),
    '#checkout' => $checkout_link,
    '#clear' => $clear_link,
    '#attached' => array(
      'js' => array(
        $ex_path . '/js/excart.js'
      ),
      'css' => array(
        $ex_path . '/css/excart.css'
      ),
      'library' => array(
        array('system', 'drupal.ajax')
      )
    )
  );
  drupal_alter('excart_cart_view', $output_array);
  return $output_array;
}

function excart_cart_update($action, $id, $count = 1) {
  $cart = excart_cart_action($action, $id, $count);
  if (isset($cart['items'][$id])) {
    if ($action == 'add') {
      $message = _excart_t('Product added to cart');
    }
    else {
      $message = _excart_t('Cart updated');
    }
    drupal_set_message($message);
    drupal_goto('node/' . $cart['items'][$id]['nid'], array(), 301);
  }
  else {
    drupal_goto('<front>', array(), 301);
  }
}

function excart_ajax_callback($action, $id, $mode, $count = 1) {
  if ($mode == 'nojs') {
    drupal_set_message(t('Plesae switch on JavaScript'));
    excart_cart_update($action, $id);
  }
  if (!empty($_GET['qty'])) {
    $count = $_GET['qty'];
  }
  $commands = array();
  $cart = excart_cart_action($action, $id, $count);

  $cart_block = excart_get_cart_block();
  $commands[] = ajax_command_replace('#excart-cart-block', render($cart_block));
  $commands[] = ajax_command_remove('.excart-upd-btn');
  // On cart page
  if (empty($cart['items'])) {
    $commands[] = ajax_command_html('#block-system-main .content', _excart_t('Cart is empty'));
  }
  else {
    $total = excart_get_total_price();
    $count = excart_get_total_count();
    $total_formatted = excart_format_total($count, $total);

    $commands[] = ajax_command_replace('#excart-total-price', render($total_formatted));

    if (!empty($cart['items'][$id])) {
      $item = $cart['items'][$id];
      $item_total = excart_format_price($item['qty'] * $item['price']);
      $commands[] = ajax_command_invoke('input[name="qty-' . $id . '"]', 'val', array($item['qty']));
      $commands[] = ajax_command_html('#excart-total-' . $id, render($item_total));
    }
    else {
      $commands[] = ajax_command_remove('#excart-row-' . $id);
    }
  }
  if (empty($_GET['finish-text']) || $_GET['finish-text'] !== 'no') {
    $finish = array(
      '#prefix' => '<div class="finish">',
      '#suffix' => '</div>',
      'content' => excart_finish_block_content($action, $item),
    );
    $commands[] = ajax_command_remove('.finish');
    $commands[] = ajax_command_append('body', render($finish));
  }


  // Allow other modules to alter ajax commands.
  drupal_alter('excart_ajax_action', $commands, $action, $id);

  return array('#type' => 'ajax', '#commands' => $commands);
}
