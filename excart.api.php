<?php

/**
 * @file
 * Hooks provided by ExCart module.
 */
/**
 * @addtogroup hooks
 * @{
 */

/**
 * Add product image to cart.
 *
 * Implements hook_excart_cart_view_alter().
 */
function hook_excart_cart_view_alter(&$output_array) {
  $image_field_name = 'field_image';
  $image_style = 'thumbnail';
  foreach ($output_array['#products'] as $id => $product) {
    $output_array['#products'][$id]['image'] = excart_get_image($product['node']->{$image_field_name}, $image_style);
  }
}

/**
 * Function hook_excart_item_id_alter allow to change id, before adding to cart.
 *
 * E.g. alter item id for appliing sale to 3rd and more product of cart.
 */
function hook_excart_item_id_alter(&$id, $nid) {
  $item = excart_get_cart_item();
  if ($item && $item['qty'] > 2) {
    $id = 'sale-' . $nid;
  }
}

/*
 * @} End of "addtogroup hooks".
 */
