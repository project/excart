<div id="cart-block">
  <?php if ($show['price']) : ?>
    <div class="total"><?php print $total_html; ?></div>
  <?php endif; ?>
  <?php if ($show['qty']) : ?>
    <div class="count"><?php print $count; ?></div>
  <?php endif; ?>
  <div class="links">
    <a href="/cart">Goto cart</a>
  </div>
</div>