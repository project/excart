<?php

function excart_order_admin_orders() {
  return drupal_get_form('excart_order_settings_form');
}

function excart_order_settings_form($form, $form_state) {
  $form['excart_order_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Manager Email address'),
    '#default_value' => variable_get('excart_order_email', '')
  );
  $form['excart_order_message'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('excart_order_message', ''),
    '#title' => t('Order complate message'),
    '#rows' => 4,
  );
  $form['excart_order_cmessage'] = array(
    '#type' => 'textarea',
    '#title' => t('Client meessage on order complete'),
    '#default_value' => variable_get('excart_order_cmessage', ''),
    '#rows' => 6
  );
  $form['excart_order_amessage'] = array(
    '#type' => 'textarea',
    '#title' => t('Admin meessage on order complete'),
    '#default_value' => variable_get('excart_order_amessage', ''),
    '#rows' => 6
  );
  $form['tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tokens'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  $form['tokens']['info'] = array(
    '#type' => 'item',
    '#title' => t('Tokens'),
    '#markup' => t('You can use tokens <br>
            !order_id - Order id <br>
            !order_link - Link to order<br>
            !site_name - Site name <br>
            !site_url - Site url<br>
            !order_items - Order<br>
            !order_info - Delivery info<br>
            !delivery_type - Delivery Type<br>
            !order_total
            ')
  );
  return system_settings_form($form);
}

function excart_order_page($archive = FALSE) {
  $oids = db_select('excart_orders', 'o')
          ->fields('o', array('oid'))
          ->orderBy('o.oid', 'DESC')
          ->execute()->fetchCol();

  $out = array();
  $rows = array();
  $row_table = array();

  foreach ($oids as $oid) {
    $items = array();
    $order = excart_order_load($oid);
    $items[] = l(t('Details'), 'admin/orders/view/' . $order->oid);
    $items[] = l(t('Order complate'), 'admin/orders/complate/' . $order->oid);
    $links = theme('item_list', array(
      'items' => $items,
      'type' => 'ul'
    ));
    $status = isset($order->status) ? $order->status : 0;
    if (empty($order->data['name'])) {
      $order->data['name'] = t('Unknown');
    }
    $row_table[] = array(
      $oid,
      format_date($order->created, 'custom', 'd.m.Y - h:i'),
      $order->data['name'],
      $order->total . ' руб.',
      excart_order_get_status($status),
      $links
    );
  }
  $headers = array(
    '№',
    t('Post date'),
    t('Name'),
    t('Order total price'),
    t('Status'),
    t('Actions')
  );
  return array(
    '#theme' => 'table',
    '#rows' => $row_table,
    '#header' => $headers
  );
}

function excart_single_order_page($oid) {
  $query = false;
  $order = excart_order_load($oid);
  $table = excart_order_creat_table($order->data);
  $items = excart_order_creat_table($order->items, 'data');
  $out = array(
    'items' => array(
      'label' => array(
        '#prefix' => '<h3>',
        '#suffix' => '</h3>',
        '#markup' => 'Список товаров'
      ),
      'data' => array(
        '#markup' => $items
      ),
      'total' => array(
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#markup' => t('Total: !total', array('!total' => excart_format_price($order->total)))
      )
    ),
    'table' => array(
      'label' => array(
        '#prefix' => '<h3>',
        '#suffix' => '</h3>',
        '#markup' => 'Информация о клиенте'
      ),
      'data' => array(
        '#markup' => $table
      )
    ),
  );
  $out['actions'] = array(
     '#type' => 'fieldset',
     '#title' => t('Change order'),
     'form' => drupal_get_form('excart_order_form_action', $order),
   );
   $out['reurn'] = array(
     '#theme' => 'link',
     '#text' => t('Return to orders list'),
     '#path' => 'admin/orders',
     '#options' => array(
       'attributes' => array(),
       'html' => FALSE,
     )
   );
  return $out;
}

function excart_order_complate_page($oid) {
  $query = db_update('excart_orders')
      ->fields(array(
        'status' => 1
      ))
      ->condition('oid', $oid)
      ->execute();
  drupal_set_message(t('Order #!num closed.', array('!num' => $oid)));
  drupal_goto('admin/orders');
}

function excart_order_archive_page() {
  $table['link'] = array('#markup' => l(t('All orders'), 'admin/orders'));
  $table['table'] = excart_order_page(TRUE);
  return $table;
}
