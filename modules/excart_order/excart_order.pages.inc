<?php

function excart_order_checkout_page($show_table = TRUE) {
  $cart = excart_get_cart();
  if ($show_table) {
    $items = $cart['items'];
    foreach ($items as $id => $item) {
      $rows[$id] = array(
        'data' => array(
          array('data' => $item['title'], 'class' => array('col-title')),
          array('data' => $item['qty'], 'class' => array('col-qty')),
          array('data' => excart_format_price($item['price']), 'class' => array('col-price')),
          array('data' => excart_format_price($item['qty'] * $item['price']), 'class' => array('col-total-price')),
        ),
      );
    }

    $table = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(
        _excart_t('Product name'),
        _excart_t('Qty'),
        _excart_t('Price'),
        _excart_t('Total'),
      )
    );
    $out['table'] = $table;
    $out['table']['#prefix'] = '<div class="table-label">' . _excart_t('Shopping list') . '</div>';
    $out['table']['#suffix'] = '<div class="table-total">' . _excart_t('Total: !sum rub', array('!sum' => $cart['total'])) . '</div>';
  }
  $out['plugins'] = excart_order_get_plugins($cart);
  drupal_alter('excart_order_checkout_data', $out, $items);
  return $out;
}

function excart_order_checkout_complete() {
  $cart = excart_get_cart();
  $order_id = $cart['oid'];
  $order = excart_order_load($order_id);
  $message = excart_order_get_message($order);
  excart_clear_cart();
  return $message;
}

function excart_order_check_page() {
  $query = db_select('excart_orders', 'o')
      ->condition('o.status', 0)
      ->fields('o', array('oid'));
  $count = $query->countQuery()->execute()->fetchField();
  if (isset($_POST['ajax'])) {
    drupal_json_output(array('count' => $count));
    exit;
  }
  return $count;
}
